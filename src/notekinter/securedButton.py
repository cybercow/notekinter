
import tkinter
import tkinter.ttk
from .appPart import AppPart

class SecuredButton(tkinter.ttk.Frame, AppPart):
    def __init__(self, master, command, **kw ):
        tkinter.ttk.Frame.__init__(self, master, **kw)
        AppPart.__init__(self, master.get_appRoot())
        self.command = command
        self.mainButton = tkinter.ttk.Button( self, text = "delete all", command = self.execute )
        self.mainButton.state(['disabled'])
        self.lockLeft = tkinter.ttk.Button(self, command = self.unlock_right, text = ">")
        self.lockRight = tkinter.ttk.Button(self, command = self.unlock, text = "<",)
        self.lockRight.state(['disabled'])
        self.rowconfigure(0, weight = 1 )
        self.columnconfigure(1, weight = 1 )
        self.lockLeft.grid( row = 0, column = 0, sticky = "nswe")
        self.mainButton.grid( row = 0, column = 1, sticky = "nswe")
        self.lockRight.grid( row = 0, column = 2, sticky = "nswe")
        
    def unlock(self):
        self.mainButton.state(['!disabled'])
    
    def unlock_right(self):
        self.lockRight.state(['!disabled'])
        self.lockLeft.after(4000, self.abort_unlock)
        
    def execute(self):
        self.command()
        self.lock()
        
    def lock(self):
        self.mainButton.state(['disabled'])
        self.lockRight.state(['disabled'])

    def abort_unlock(self):
        self.lock()
