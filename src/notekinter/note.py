
from .appPart import AppPart
import tkinter
import tkinter.ttk
'''
Author: Stefan Pasteiner
License: AGPL v 3 or any later like published by the fsf on gnu.org
'''

class Preview(tkinter.ttk.Frame, AppPart):
    def __init__(self, master:AppPart, note):
        AppPart.__init__(self, master.get_appRoot())
        tkinter.ttk.Frame.__init__(self, master)
        self.note = note
        
class TextPreview(Preview):
    def __init__(self, master:AppPart, note):
        Preview.__init__(self, master, note)
        self.main_label = tkinter.ttk.Label( self, text= self.note.text, wraplength=self.get_appRoot().mainFrame.winfo_width())
        self.main_label.grid(row = 0, column = 0, sticky="nsew")
        
class Note:
    def __init__(self):
        self.__preview = None
        self.path = None
    
    def get_preview(self) -> Preview:
        return self.__preview
    
    def make_preview(self, master:AppPart) -> Preview:
        raise NotImplementedError("This method acts as placeholder, like interface methods in other languages")
    
class TextNote( Note ):
    def __init__(self, text:str):
        #Note.__init__(self)
        self.__preview = None
        self.text = text
        self.path = None
        
    def make_preview(self, master:AppPart):
        if self.__preview != None:
            raise ValueError("preview already exists")
        self.__preview = TextPreview(master, self)
        return self.__preview
    
class PreviewContainer( tkinter.ttk.Frame, AppPart ):
    def __init__(self, master: AppPart):
        AppPart.__init__(self, master.get_appRoot())
        tkinter.ttk.Frame.__init__(self, master)
        self.previews = list()
    
    def append(self, note:Note):
        preview = note.make_preview(self)
        preview.grid(column = 0, sticky = "we")
        self.previews.append( preview )
        
    def clear(self):
        for element in self.previews:
            element.grid_forget()
        self.previews = list()
    
