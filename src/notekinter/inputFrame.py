'''
Author: Stefan Pasteiner
License: AGPL v 3 or any later like published by the fsf on gnu.org
'''

from .appPart import AppPart
import tkinter
import tkinter.ttk
from . import note
from . import securedButton

class InputFrame(tkinter.ttk.Frame, AppPart):
    def __init__(self, master:AppPart):
        AppPart.__init__(self, master.get_appRoot())
        tkinter.ttk.Frame.__init__(self, master)
        self.textInput = tkinter.StringVar()
        self.entry = tkinter.ttk.Entry(self, textvariable = self.textInput )
        self.entry.grid( row = 0, column = 0, sticky = "ew", columnspan = 2 )
        self.button = tkinter.ttk.Button(self, text = "OK", command = self.text_to_note)
        self.button.grid(column = 0, row = 1, sticky = "e" )
        self.columnconfigure( 0, weight = 1 ) 
        self.delButton = securedButton.SecuredButton(self, command = self.get_appRoot().delete_all)
        self.delButton.grid(column = 1, row = 1)

        self.entry.bind('<Return>', self.text_to_note)
        
    def text_to_note(self, event = None):
        text = self.textInput.get()
        _note = note.TextNote(text)
        self.textInput.set("")
        self.get_appRoot().add_note(_note, True)
        self.entry.focus()
