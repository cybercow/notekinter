'''
Author: Stefan Pasteiner
License: AGPL v 3 or any later like published by the fsf on gnu.org
'''

import os
import time
from .note import TextNote

class Context:
    def __init__(self, args:list = None):
        if args != None:
            raise NotImplementedError("Programm options are still not implemented")
        
        self.name = "notekinter"
        self.noteDirPath = os.path.expanduser("~"+os.sep+"notekinter"+os.sep)
        
    def load_saved(self, appRoot):
        try:
            filenames = os.listdir(self.noteDirPath)
            filenames.sort()
            for filename in filenames:
                with open( self.noteDirPath + filename, "r" ) as file:
                    content = file.read()
                    note = appRoot.add_note(content)
                    note.path = self.noteDirPath + filename
        except FileNotFoundError:
            os.mkdir(self.noteDirPath)
                
    def save_text( self, textNote ):
        if type( textNote ) == str:
            textNote = TextNote( textNote )
        path =  self.noteDirPath + "n" + str(time.time())
        with open(path, "w" ) as file:
            file.write( textNote.text )
            textNote.path = path
        return textNote
