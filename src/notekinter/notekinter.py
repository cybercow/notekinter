'''
Author: Stefan Pasteiner
License: AGPL v 3 or any later like published by the fsf on gnu.org
'''

import tkinter
import tkinter.ttk
from .appPart import AppPart
from .inputFrame import InputFrame
from . import note
from . import mainFrame
from . import context
import os



class Notekinter( tkinter.ttk.Frame, AppPart ):
    def __init__(self, cont:context.Context, master = None, config = None ):
        AppPart.__init__(self, self)
        self.context = cont
        if master == None:
            master = tkinter.Tk()
            master.columnconfigure(0, weight = 1)
            master.rowconfigure(0, weight = 1)
        tkinter.ttk.Frame.__init__(self, master)
        self.inputFrame = InputFrame(self)
        self.inputFrame.grid(row = 1, column = 0, sticky = "ew")
        self.mainFrame = mainFrame.MainFrame(self)
        self.mainFrame.grid( row = 0, column = 0, sticky = "nwes" )
        self.scrollbar_y = tkinter.ttk.Scrollbar(self, orient = tkinter.VERTICAL, command = self.mainFrame.yview)
        self.scrollbar_y.grid( row = 0, column = 1, sticky = "ns" )
        self.mainFrame['yscrollcommand'] = self.get_appRoot().scrollbar_y.set
        self.mainFrame.create_window(0,0, window = self.mainFrame.previewContainer, anchor="nw")
        self.mainFrame.previewContainer.bind("<Configure>", self.update_scrollregion)
        self.notes = list()
        self.columnconfigure(0, weight = 1)
        self.rowconfigure(0, weight = 1)

    def update_scrollregion(self, event):
        self.mainFrame.configure(scrollregion=self.mainFrame.bbox("all"))
        
    def add_note(self, _note:note.Note, save_note:bool = False ):
        if type(_note) == str:
            _note = self.add_note(note.TextNote(_note ), save_note )
        else:
            self.notes.append(_note)
            self.mainFrame.previewContainer.append(_note)
            if save_note:
                _note.path = self.context.save_text( _note ).path
        return _note
        
    def delete_all(self):
        for element in self.notes:
            os.remove(element.path)
        self.mainFrame.previewContainer.clear()
        self.notes = list()
        
if __name__ == "__main__":
    cont = context.Context()
    app = Notekinter(cont)
    app.grid(column = 0, row = 0, sticky = "nsew")
    app.inputFrame.entry.focus()
    app.master.mainloop()

    
