from .notekinter import Notekinter
from . import context


def run():
    cont = context.Context()
    app = Notekinter(cont)
    app.grid(column = 0, row = 0, sticky = "nsew")
    app.inputFrame.entry.focus()
    app.master.update()
    app.context.load_saved(app)
    app.master.mainloop()

if __name__ == "__main__":
    run()
