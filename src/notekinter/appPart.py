'''
Author: Stefan Pasteiner
License: AGPL v 3 or any later like published by the fsf on gnu.org
'''

import tkinter

class AppPart(tkinter.Widget):
    def __init__(self, appRoot):
        self.__appRoot = appRoot
        
    def get_appRoot(self):
        return self.__appRoot
