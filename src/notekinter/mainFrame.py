'''
Author: Stefan Pasteiner
License: AGPL v 3 or any later like published by the fsf on gnu.org
'''

from .appPart import AppPart
import tkinter
import tkinter.ttk
from . import note

class MainFrame(tkinter.Canvas, AppPart):
    def __init__(self, master:AppPart):
        AppPart.__init__(self, master.get_appRoot())
        tkinter.Canvas.__init__(self, master)
        self.previewContainer = note.PreviewContainer(self)
        #self.previewContainer.grid( row = 0, column = 0, sticky = "nwes" )
        self.rowconfigure(0, weight = 1)
        self.columnconfigure(0, weight = 1)
