# notekinter

A minimal note taking app.

# Installation and usage:

This repository can be installed by executing the command
python3 -m pip install .
in the root directory of this git repository
after that the program can be started by executing
python3 -m notekinter

This program is written und used by the author
exclusively under Linux.
It may runs under other Systems but
that's not the authors fault.

# development

For development you can ether run
python3 -m notekinter
from the 'src' directory without installing anything
or install it as editable package by executing the command
python3 -m pip install -e .
from the root directory
